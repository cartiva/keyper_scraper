program KeyperScrape;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  KeyperDM in 'KeyperDM.pas' {DM: TDataModule};

resourcestring
  executable = 'Hourly';


begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
//      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
//        QuotedStr(executable) + ', ' +
//        '''' + 'none' + '''' + ', ' +
//        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
// *** active procs ***//
      DM.KeyperScrape;
// *** active procs ***//
//      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
//        QuotedStr(executable) + ', ' +
//        '''' + 'none' + '''' + ', ' +
//        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//        'null' + ')');
    except
      on E: Exception do
      begin
//        DM.ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
//          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//          '''' + 'ServicleLine' + '''' + ', ' +
//          '''' + 'no sql - line 136' + '''' + ', ' +
//          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
//
//        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
//          QuotedStr(executable) + ', ' +
//          '''' + 'none' + '''' + ', ' +
//          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
//
//        DM.SendMail('ServiceLine.' + leftstr(E.Message, 8),   E.Message);
//        exit;
      end;
    end;
//    DM.SendMail('ServiceLine passed');
  finally
    FreeAndNil(DM);
  end;
end.
